﻿using AutoMapper;
using HomeCinema.Data.Entities;
using HomeCinema.Services.Dto;
using MapperProfiles.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MapperProfiles
{
	public class MapperProfile : Profile
	{
		public MapperProfile()
		{
			//Data to BLL
			CreateMap<Actor, ActorDTO>();
			CreateMap<Film, FilmDTO>()
				.ForMember(x=>x.GenreFilms,conf=>conf.MapFrom(x=>x.GenreFilms))
				.ForMember(x => x.ActorFilms, conf => conf.MapFrom(x => x.ActorFilms));
			CreateMap<Genre, GenreDTO>();
			CreateMap<Image, ImageDTO>();
			CreateMap<ActorFilms, ActorFilmsDTO>();
			CreateMap<GenreFilms, GenreFilmsDTO>();
			CreateMap<Genre, GenreFilms>();

			//BLL to Web
			CreateMap<GenreDTO, GenreViewModel>();
			CreateMap<ActorDTO, ActorViewModel>();
			CreateMap<FilmDTO, FilmViewModel>()
				.ForMember(y => y.Actors, spc => spc.MapFrom(x => x.ActorFilms.Select(x => x.Actor).ToList()))
				.ForMember(x => x.Genres, spc => spc.MapFrom(x => x.GenreFilms.Select(x => x.Genre).ToList()));
			CreateMap<GenreDTO, GenreViewModel>();
			CreateMap<ImageDTO, ImageViewModel>();


			//Bll To Data
			CreateMap<ActorDTO, Actor>();
			CreateMap<ActorFilmsDTO, ActorFilms>();
			CreateMap<GenreFilmsDTO, GenreFilms>();
			CreateMap<FilmDTO, Film>();
			CreateMap<GenreDTO, Genre>();
			CreateMap<ImageDTO, Image>();

			//Web To BLL
			CreateMap<FilmForAddOrEditViewModel, FilmDTO>();
			
		}
	}
}
