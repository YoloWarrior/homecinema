﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace MapperProfiles.Models
{
	public class FilmViewModel
	{
		public string Name { get; set; }
		public string PreviousName { get; set; }
		public string Description { get; set; }
		public int YearOfRelease { get; set; }
		public string Producer { get; set; }
		public Guid UserId { get; set; }
		public ImageViewModel Image { get; set; }
		public virtual List<ActorViewModel> Actors { get; set; }
		public virtual List<GenreViewModel> Genres { get; set; }
	}
}
