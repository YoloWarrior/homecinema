﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MapperProfiles.Models
{
	public class ImageViewModel
	{
		public string ImageLink { get; set; }
	}
}
