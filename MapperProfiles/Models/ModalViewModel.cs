﻿using HomeCinema.Services.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace MapperProfiles.Models
{
	public class ModalViewModel
	{
		public string Text { get; set; }
		public ModalType Type { get; set; }
	}
}
