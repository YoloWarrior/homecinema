﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MapperProfiles.Models
{
	public class FilmForAddOrEditViewModel
	{
		public string Name { get; set; }
		public string PreviousName { get; set; }
		public string Description { get; set; }
		public int YearOfRelease { get; set; }
		public string Producer { get; set; }
		public string Actors { get; set; }
		public string Genres { get; set; }
		public Guid UserId { get; set; }
	}
}
