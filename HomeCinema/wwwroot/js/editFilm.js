var prevName = $('#film-name').text();
 var filmName = $('#film-name').text();

$(".editButton").click(function(event) {
    editMode = true;

    changeButtonsForEdit();

    $('#film-name').replaceWith('<input type="text" name="filmName" value="' + filmName + '"  class="edit-name"/>');
    $('#description').replaceWith('<textarea>' + $('#description').html() + '</textarea>');
    $(".film-logo").children("img").css("opacity", "0.3")
    $('img.plus-actor').show();
    $('img.plus-genre').show();
    $(".film-logo").append('<h4 class="edit-image-text">Кликните для изменения</h4>')
    $(".image-film").css("cursor", "pointer");
});




document.addEventListener('DOMContentLoaded', function() {

    $("#select-image").change(function() {
        readURL(this);
    });

});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {

            $('#film-poster').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}


$("#film-poster").click(function(event) {
    if (editMode || isAddMode)
        $('#select-image').click();
})

function changeButtonsForEdit() {

    if ($('#save-btn').is(":hidden")) {
        $('#edit-btn').hide();
        $('#save-btn').show();
        $('#cancel-btn').show();
    } else {
        $('#edit-btn').show();
        $('#save-btn').hide();
        $('#cancel-btn').hide();
    }

}


$(function() {
    $("#edit-film").submit(function(e) {
        e.preventDefault();

        var formAction = $(this).attr("action");
        var fdata = new FormData();

        var actors = new Array();
        var genres = [];
        var actorsList = $('a.actor-text');
        var genresList = $('a.genre-text')

        actorsList.each(function(ind, elem) {
            actors.push(elem.text)
        });

        genresList.each(function(index, el) {
            genres.push(el.text)
        });

        var fileInput = $('#select-image')[0];
        var file = fileInput.files[0];

        if(file == undefined){
              infoModal('Вы не выбрали постер к фильму','Warning')
        }else if($('input.edit-name').val() == undefined){
              infoModal('Вы не указали название фильма','Warning')
          }
        else{
        fdata.append("file", file);
        fdata.append('name', $('input.edit-name').val())
        fdata.append('previousName', prevName)
        fdata.append('yearOfRelease', $('a.yearRelease-text').text())
        fdata.append('description', $('textarea').text())
        fdata.append('producer', $('a.produccer-text').text())
        fdata.append('actors', JSON.stringify(actors))
        fdata.append('genres', JSON.stringify(genres))

        $.ajax({
            type: 'post',
            url: formAction,
            data: fdata,
            processData: false,
            contentType: false
        }).done(function(result) {
           
            if (result.status === "success") {
              infoModal('Вы успешно добавили фильм','Info')
            } else {
                
            }
        });
        }
        
    });
});