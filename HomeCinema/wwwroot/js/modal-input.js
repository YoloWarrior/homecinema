var editMode = false;
var isAddMode = false;
var textModalInput = 'Вы хотите добавить'
var textModalEdit = 'Вы хотите изменить'
var modalText = '';
var inputValue = '';
var inputType = ''
var isReplace;
var isNumberType = false;

function changeFilmInfo(item, text,isReplaceValue,numberType) {
	isReplace = isReplaceValue;
    inputType = item;
    modalText = text;
    isNumberType = numberType;
    let modalTextSummary = isReplace == true ? textModalEdit : textModalInput ;
    $('.modal-input p').replaceWith('<p>' +modalTextSummary+ ' ' + text + '</p>');


    changeModalState();
}

function addOrEditInfo() {
    let value = $('input.modal-text').val();

    var link = createNewElement();

    var linkText = document.createTextNode(value);
    link.appendChild(linkText);

    link.addEventListener('click', function(target) {
        if (editMode || isAddMode)
            target.target.remove();

    }, false);

    $(link).addClass(inputType + '-text');

    if(!isReplace){
    	 $('p#' + inputType + '-list').children('img').before(link);
    	 
    	 if(editMode)
    	 		WorkWithEntity(inputType == 'actor' ? true:false,value,filmName ,false)
    }
		else
   		$('a.' + inputType + '-text').text(value)


    changeModalState();
}

function createNewElement() {
    var a = document.createElement('a');
    a.href = "#";

    return a;
}

function checkIfNewElementExistInCollection(typeName, val) {
    let isExist = false;

    var elems = $('p#' + typeName + '-list').children('a').each(function() {
        let elem = this.text.toLowerCase().trim();
        let checkVal = val.trim().toLowerCase();

        if (elem === checkVal) {
            isExist = true;
            return;
        }
    })

    return isExist;
}

$("input.modal-text").on("keyup", function(event) {
    inputValue = event.target.value;
    disableSaveButtonIfCondition(checkIfNewElementExistInCollection(inputType, inputValue));
});


function disableSaveButtonIfCondition(condition) {
    var elem = $('#modal-saveBtn');

    if (condition) {
        $(elem).prop('disabled', true);
    } else {
        $(elem).prop('disabled', false);
    }

}

function changeModalState() {

		 $('input.modal-text').val('');

	if(isNumberType)
	    $('input.modal-text').prop({type:"number"})
	else
		 $('input.modal-text').prop({type:"text"})


    let element = $('div.modal-input');

    if ($(element).is(":hidden"))
        $(element).show();
    else
        $(element).hide();
	
   
}

$(".genre-text").click(function(event) {
    if (editMode)
    {
        WorkWithEntity(false,event.target.text,filmName ,true)
    }
})

$(".actor-text").click(function(event) {
    if (editMode){
        WorkWithEntity(true,event.target.text,filmName ,true)
    }
})


function addMode(){
	isAddMode = true;
}