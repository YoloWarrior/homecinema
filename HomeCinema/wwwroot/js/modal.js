function infoModal(text,type){
    $.ajax({
                    url: '/Modal/InfoModal',
                    data:{text:text,type:type},
                    cache: false,
                    type: "Get",
                    success: function (data) {
                       $('body').append(data)
                       $('.modal-info').show();
                       
                       removeModal('.modal-info')
                }
})
}    

function removeModal(modal) {
    setTimeout(() => $(modal).hide("slow"), 2000);
}
    