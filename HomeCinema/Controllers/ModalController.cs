﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HomeCinema.Services.Helpers;
using MapperProfiles.Models;
using Microsoft.AspNetCore.Mvc;

namespace HomeCinema.Controllers
{
    public class ModalController : Controller
    {
        public  PartialViewResult InfoModal(string text,ModalType type)
        {
            return PartialView("~/Views/_Partial/InfoModal.cshtml", new ModalViewModel { Text = text, Type = type });
        }
    }
}