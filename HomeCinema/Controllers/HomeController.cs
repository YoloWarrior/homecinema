﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using HomeCinema.Services.Dto;
using HomeCinema.Services.Interfaces;
using MapperProfiles.Models;
using Microsoft.AspNetCore.Mvc;


namespace HomeCinema.Controllers
{
	public class HomeController : Controller
	{
		private int pageSize = 10;
		private IMapper _mapper;
		private IServiceWithInclude<FilmDTO> _filmService;

		public HomeController(IServiceWithInclude<FilmDTO> filmService,IMapper mapper)
		{
			_mapper = mapper;
			_filmService = filmService;
		}

		public IActionResult EditFilm(FilmViewModel film)
		{
			_filmService.Edit(_mapper.Map<FilmDTO>(film));

			return Ok();
		}

		public IActionResult GetSingleFilm(string filmName)
		{
			var film = _filmService.GetWithInclude(filmName);

			return View("~/Views/Film/Single.cshtml", 
				_mapper.Map<FilmViewModel>(film));
		}

		public async Task<IActionResult> Index(int page = 0)
		{
		var films = await _filmService.GetAllWithInclude(x => x.Image);

			if (page != 0)
				films = films.Skip(page * pageSize).Take(page * pageSize).ToList();

			ViewBag.PageCount = 10;

			return View("~/Views/Home/Index.cshtml",_mapper.Map<IEnumerable<FilmViewModel>>(films));
		}

		[HttpGet]
		public IActionResult AddFilm()
		{
			return View("~/Views/Film/Add.cshtml");
		}

		private int PageCount(IEnumerable<FilmDTO> filmDTO)
		{
			int sum = 0;
			int step = 10;
			int skipPages = 0;

			foreach (var p in filmDTO.Skip(step).Take(step))
			{
				if (sum == 0)
				{
					skipPages += pageSize;
				}
				else
				{
					step += pageSize;
					skipPages += pageSize;
				}

				sum += 1;

			}

			return sum;
		}

	}
}
