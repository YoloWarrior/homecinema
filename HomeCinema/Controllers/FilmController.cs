﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MapperProfiles.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using HomeCinema.Services.Helpers;
using AutoMapper;
using HomeCinema.Services.Dto;
using HomeCinema.Services.Services;
using HomeCinema.Services.Interfaces;
using Microsoft.AspNetCore.Hosting;
using System.IO;

namespace HomeCinema.Controllers
{
	public class FilmController : Controller
	{
		IMapper _mapper;
		private IServiceWithInclude<FilmDTO> _filmService;
		private readonly IHostingEnvironment _hostingEnv;

		public FilmController(IMapper mapper, IServiceWithInclude<FilmDTO> filmService, IHostingEnvironment environment)
		{
			_mapper = mapper;
			_hostingEnv = environment;
			_filmService = filmService;
		}

		[HttpPost]
		public IActionResult Add(IFormFile file, FilmForAddOrEditViewModel film)
		{
			var filmDTO = GetCollectionOfGenresAndActorsFromRequest.GetFilmDTOFromRequest(_mapper.Map<FilmDTO>(film), film.Actors, film.Genres);

			if (file != null)
			{
				filmDTO.Image = SaveFilmPoster(file).Result;
			}

			filmDTO.UserId = Guid.Parse(User.GetUserId());
			int FilmId = _filmService.Create(filmDTO).Id;

			return Ok(FilmId);
		}

		[HttpGet]
		public IActionResult Add()
		{
			return View("~/Views/Film/Add.cshtml");
		}

		[HttpPost]
		public async Task<IActionResult> Edit(IFormFile file, FilmForAddOrEditViewModel film)
		{
			string prevName = film.PreviousName.Replace("\r\n", "");
			var filmDTO = _mapper.Map<FilmDTO>(film);
			filmDTO.Id = _filmService.Get(prevName).Id;

			if (file != null)
			{
				filmDTO.Image = await SaveFilmPoster(file);
			}

			_filmService.Edit(filmDTO);

			return Ok();
		}


		private async Task<ImageDTO> SaveFilmPoster(IFormFile file)
		{
			string filePath = "";
			var a = _hostingEnv.WebRootPath;
			var fileName = Path.GetFileName(file.FileName);

			filePath = Path.Combine(_hostingEnv.WebRootPath, "images\\FilmsLogos", fileName);

			using (var fileSteam = new FileStream(filePath, FileMode.Create))
			{
				await file.CopyToAsync(fileSteam);
			}

			return new ImageDTO
			{
				ImageLink = fileName
			};

		}
	}
}