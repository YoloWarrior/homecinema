﻿using HomeCinema.Services.Dto;
using HomeCinema.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace HomeCinema.Controllers
{
	public class ActorController : Controller
	{

		private IServiceWithInclude<ActorFilmsDTO> _actorFilmsService;
		private IServiceWithInclude<FilmDTO> _filmService;
		private IService<ActorDTO> _actorService;

		public ActorController(IServiceWithInclude<ActorFilmsDTO> actorFilmsService, IServiceWithInclude<FilmDTO> filmService, IService<ActorDTO> actorService)
		{
			_actorFilmsService = actorFilmsService;
			_filmService = filmService;
			_actorService = actorService;
		}

		[HttpPost]
		public IActionResult Post(string name, string filmName)
		{
			var lastItem = _actorFilmsService.GetAll().Result;
			var film = _filmService.Get(filmName);

			if (_actorService.Get(name) == null)
			{
				_actorService.Create(new ActorDTO
				{
					Name = name
				});
			}

			_actorFilmsService.Create(new ActorFilmsDTO
			{
				FilmId = film.Id,
				ActorId = _actorService.GetAll().Result.Count() == 0 ? 1 : _actorService.GetAll().Result.Last().Id,
				ActorFilmsId = lastItem.Count() == 0 ? 1 : lastItem.Last().ActorFilmsId + 1
			});

			return Ok();

		}

		[HttpDelete]
		public IActionResult Delete(string name, string filmName)
		{
			_actorFilmsService.Delete(_actorFilmsService.GetAllWithInclude().Result.Where(x => x.Film.Name.Contains(filmName) && x.Actor.Name.Contains(name)).SingleOrDefault().ActorFilmsId);

			return Ok();
		}
	}
}