﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HomeCinema.Services.Dto;
using HomeCinema.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace HomeCinema.Controllers
{
	public class GenreController : Controller
	{

		private IServiceWithInclude<GenreFilmsDTO> _genreFilmsService;
		private IServiceWithInclude<FilmDTO> _filmService;
		private IService<GenreDTO> _genreService;

		public GenreController(IServiceWithInclude<GenreFilmsDTO> genreFilmsService, IServiceWithInclude<FilmDTO> filmService, IService<GenreDTO> genreService)
		{
			_genreFilmsService = genreFilmsService;
			_filmService = filmService;
			_genreService = genreService;
		}

		[HttpPost]
		public IActionResult Post(string name, string filmName)
		{
			var lastItem = _genreFilmsService.GetAll().Result;
			var film = _filmService.Get(filmName);

			if (_genreService.Get(name) == null)
			{
				_genreService.Create(new GenreDTO
				{
					Name = name
				});
			}
			
			_genreFilmsService.Create(new GenreFilmsDTO
			{
				FilmId = film.Id,
				GenreId = _genreService.GetAll().Result.Count() == 0 ? 1 : _genreService.GetAll().Result.Last().Id,
				GenreFilmsId = lastItem.Count() == 0 ? 1 : lastItem.Last().GenreFilmsId + 1
			});

			return Ok();
		}

		[HttpDelete]
		public IActionResult Delete(string name, string filmName)
		{
			_genreFilmsService.Delete(_genreFilmsService.GetAllWithInclude().Result.Where(x => x.Film.Name.TrimEnd().Contains(filmName) && x.Genre.Name.Contains(name)).SingleOrDefault().GenreFilmsId);

			return Ok();

		}
	}
}