﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HomeCinema.Data.Entities;
using MapperProfiles.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace HomeCinema.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;

        public AccountController(UserManager<User> userManager, SignInManager<User> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

  

        [HttpPost]
        public async Task<IActionResult> Register(UserViewModel userView)
        {
            if (ModelState.IsValid)
            {
                User user = new User { FullName =  userView.Name,UserName = userView.UserName};
               
                var result = await _userManager.CreateAsync(user, userView.Password);

                if (!result.Succeeded)
                {
                 
                    return RedirectToAction("Index", "Home");
                }
               
            }

            return RedirectToAction("Login", "Account", userView);
        }

  
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(UserViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result =
                    await _signInManager.PasswordSignInAsync(model.UserName, model.Password,true,false);

                if (!result.Succeeded)
                {
                    return RedirectToAction("Index", "Home");
                }
                
            }

            return Ok(model.Name);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
     
            await _signInManager.SignOutAsync();
            return Ok();
        }
    }
}