﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeCinema.Services.Dto
{
	public class GenreDTO
	{
		public int Id { get; set; }
		public string Name { get; set; }

		public virtual List<GenreFilmsDTO> GenreFilms { get; set; }
	}
}
