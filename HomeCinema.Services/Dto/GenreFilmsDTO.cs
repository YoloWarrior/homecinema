﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeCinema.Services.Dto
{
	public class GenreFilmsDTO
	{
		public int GenreFilmsId { get; set; }
		public int GenreId { get; set; }
		public int FilmId { get; set; }
		public GenreDTO Genre { get; set; }
		public FilmDTO Film { get; set; }
	}
}
