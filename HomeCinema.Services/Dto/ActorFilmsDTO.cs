﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeCinema.Services.Dto
{
	public class ActorFilmsDTO
	{
		public int ActorFilmsId { get; set; }
		public int ActorId { get; set; }
		public int FilmId { get; set; }
		public ActorDTO Actor { get; set; }
		public FilmDTO Film { get; set; }
	}
}
