﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeCinema.Services.Dto
{
	public class ActorDTO
	{
		public int Id { get; set; }
		public string Name { get; set; }

		public virtual List<ActorFilmsDTO> ActorFilms { get; set; }
	}
}
