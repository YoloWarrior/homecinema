﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeCinema.Services.Dto
{
	public class FilmDTO
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public int YearOfRelease { get; set; }
		public string Producer { get; set; }
		public Guid UserId { get; set; }
		public ImageDTO Image { get; set; }
		public virtual List<ActorFilmsDTO> ActorFilms { get; set; }
		public virtual List<GenreFilmsDTO> GenreFilms { get; set; }
	}
}
