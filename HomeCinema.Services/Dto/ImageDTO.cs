﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeCinema.Services.Dto
{
	public class ImageDTO
	{
		public int ImageId { get; set; }
		public string ImageLink { get; set; }
	}
}
