﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace HomeCinema.Services.Interfaces
{
	public interface IService<T>where T:class
	{
		Task<IEnumerable<T>> GetAll();
		T Create(T entity);
		void Delete(int Id);
		void Edit(T entity);
		T Get(string name);
		bool Contains(T entity);
	}

	public interface IServiceWithInclude<T>:IService<T>where T : class
	{
		Task<IEnumerable<T>> GetAllWithInclude(params Expression<Func<T, object>>[] includeProperties);
		T GetWithInclude(string name);
		IEnumerable<T> FindWithInclude(Func<T, bool> predicate, params Expression<Func<T, object>>[] includeProperties);
	}
}
