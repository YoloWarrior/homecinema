﻿using AutoMapper;
using HomeCinema.Data.Entities;
using HomeCinema.Data.Interfaces;
using HomeCinema.Data.Repositories;
using HomeCinema.Services.Dto;
using HomeCinema.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace HomeCinema.Services.Services
{
	public class ActorFilmsService : IServiceWithInclude<ActorFilmsDTO>
	{
		IReposWrapper _repos;
		IMapper _mapper;
		private readonly Data.DataContext _context;

		public ActorFilmsService(IMapper mapper, IReposWrapper repos = null,Data.DataContext context = null)
		{
			_context = context ?? new Data.DataContext();
			_repos = repos ?? new ReposWrapper(new Data.DataContext());
			_mapper = mapper;
		}

		public bool Contains(ActorFilmsDTO entity) => Get(entity.Film.Name)
			!= null ? true : false;


		public ActorFilmsDTO Create(ActorFilmsDTO entity)
		{
			if (Contains(entity))
				return null;

			return _mapper.Map<ActorFilmsDTO>(_repos.ActorFilms.Create(_mapper.Map<ActorFilms>(entity)));
		}

		public void Delete(int Id)
		{
			var entity = _repos.ActorFilms.Get(x => x.ActorFilmsId == Id);

			if (entity != null)
				_repos.ActorFilms.Delete(entity);
		}

		public void Edit(ActorFilmsDTO entity)
		{

			_repos.ActorFilms.Edit(_mapper.Map<ActorFilms>(entity));
		}

		public IEnumerable<ActorFilmsDTO> FindWithInclude(Func<ActorFilmsDTO, bool> predicate, params Expression<Func<ActorFilmsDTO, object>>[] includeProperties)
		{
			return _mapper.Map<IEnumerable<ActorFilmsDTO>>
				(_repos.ActorFilms.FindWithInclude(_mapper.Map<Func<ActorFilms, bool>>(predicate),
				_mapper.Map<Expression<Func<ActorFilms, object>>>(includeProperties)));
		}

		public ActorFilmsDTO Get(string name) =>
			_mapper.Map<ActorFilmsDTO>(_repos.ActorFilms.Get(x => x.Film.Name == name)) ?? null;

		public async Task<IEnumerable<ActorFilmsDTO>> GetAll() =>
			_mapper.Map<IEnumerable<ActorFilmsDTO>>(await _repos.ActorFilms.GetAll());


		public async Task<IEnumerable<ActorFilmsDTO>> GetAllWithInclude(params Expression<Func<ActorFilmsDTO, object>>[] includeProperties)
		{
			return _mapper.Map<IEnumerable<ActorFilmsDTO>>(await _context.ActorFilms.Include(x => x.Film).Include(x=>x.Actor).ToListAsync());
		}

		public ActorFilmsDTO GetWithInclude(string name)
		{
			return null;
		}
	}
}
