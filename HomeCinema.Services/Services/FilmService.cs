﻿using AutoMapper;
using HomeCinema.Data.Entities;
using HomeCinema.Data.Interfaces;
using HomeCinema.Data.Repositories;
using HomeCinema.Services.Dto;
using HomeCinema.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace HomeCinema.Services.Services
{
	public class FilmService : IServiceWithInclude<FilmDTO>
	{
		private readonly IReposWrapper _repos;
		private readonly IMapper _mapper;
		private readonly Data.DataContext _context;

		public FilmService(IMapper mapper, IReposWrapper repos = null, Data.DataContext context = null)
		{
			_repos = repos ?? new ReposWrapper(new Data.DataContext());
			_context = context ?? new Data.DataContext();
			_mapper = mapper;
		}

		public bool Contains(FilmDTO entity) => Get(entity.Name)
			!= null ? true : false;


		public FilmDTO Create(FilmDTO entity)
		{
			if (Contains(entity))
				return null;

			return _mapper.Map<FilmDTO>(_repos.Film.Create(_mapper.Map<Film>(entity)));

		}

		public void Delete(int Id)
		{
			var entity = _repos.Film.Get(x => x.Id == Id);

			if (entity != null)
				_repos.Film.Delete(entity);
		}

		public void Edit(FilmDTO entity)
		{
			_repos.Film.Edit(_mapper.Map<Film>(entity));
		}

		public IEnumerable<FilmDTO> FindWithInclude(Func<FilmDTO, bool> predicate, params Expression<Func<FilmDTO, object>>[] includeProperties)
		{
			return _mapper.Map<IEnumerable<FilmDTO>>
				(_repos.Film.FindWithInclude(_mapper.Map<Func<Film, bool>>(predicate),
				_mapper.Map<Expression<Func<Film, object>>>(includeProperties)));
		}

		public FilmDTO Get(string name) =>
			_mapper.Map<FilmDTO>(_repos.Film.Get(x => x.Name.Contains(name))) ?? null;

		public async Task<IEnumerable<FilmDTO>> GetAll() =>
			_mapper.Map<IEnumerable<FilmDTO>>(await _repos.Film.GetAll());

		//TODO подумать как сделать мапер для дерева
		public async Task<IEnumerable<FilmDTO>> GetAllWithInclude(params Expression<Func<FilmDTO, object>>[] includeProperties)
		{
			return _mapper.Map<IEnumerable<FilmDTO>>(await _context.Films.Include(x => x.Image).ToListAsync());
		}

		public FilmDTO GetWithInclude(string name)
		{
			var entity = _context.Films
				.Include(x => x.Image)
				.Include(x => x.GenreFilms)
				.ThenInclude(x => x.Genre)
				.Include(x => x.ActorFilms)
				.ThenInclude(x => x.Actor)
				.Where(x => x.Name.Contains(name))
				.SingleOrDefault();

			return _mapper.Map<FilmDTO>(entity);

		}
	}
}
