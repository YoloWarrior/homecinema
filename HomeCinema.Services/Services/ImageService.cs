﻿using AutoMapper;
using HomeCinema.Data.Entities;
using HomeCinema.Data.Interfaces;
using HomeCinema.Data.Repositories;
using HomeCinema.Services.Dto;
using HomeCinema.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HomeCinema.Services.Services
{
	public class ImageService : IService<ImageDTO>
	{
		IReposWrapper _repos;
		IMapper _mapper;

		public ImageService(IMapper mapper, IReposWrapper repos = null)
		{
			_repos = repos ?? new ReposWrapper(new Data.DataContext());
			_mapper = mapper;
		}

		public bool Contains(ImageDTO entity) => Get(entity.ImageLink)
			!= null ? true : false;


		public ImageDTO Create(ImageDTO entity)
		{
			if (Contains(entity))
				return null;

				return _mapper.Map<ImageDTO>(_repos.Image.Create(_mapper.Map<Image>(entity)));
		}

		public void Delete(int Id)
		{
			var entity = _repos.Image.Get(x => x.ImageId == Id);

			if (entity != null)
				_repos.Image.Delete(entity);
		}

		public void Edit(ImageDTO entity)
		{
			var ent = Get(entity.ImageLink);

			if (ent != null)
				_repos.Image.Edit(_mapper.Map<Image>(entity));
		}


		public ImageDTO Get(string name) =>
			_mapper.Map<ImageDTO>(_repos.Image.Get(x => x.ImageLink == name)) ?? null;

		public async Task<IEnumerable<ImageDTO>> GetAll() =>
			_mapper.Map<IEnumerable<ImageDTO>>(await _repos.Image.GetAll());

	}
}
