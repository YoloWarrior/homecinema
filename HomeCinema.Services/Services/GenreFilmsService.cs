﻿using AutoMapper;
using HomeCinema.Data.Entities;
using HomeCinema.Data.Interfaces;
using HomeCinema.Data.Repositories;
using HomeCinema.Services.Dto;
using HomeCinema.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace HomeCinema.Services.Services
{
	public class GenreFilmsService : IServiceWithInclude<GenreFilmsDTO>
	{
		IReposWrapper _repos;
		IMapper _mapper;
		private readonly Data.DataContext _context;

		public GenreFilmsService(IMapper mapper, IReposWrapper repos = null, Data.DataContext context = null)
		{
			_context = context ?? new Data.DataContext();
			_repos = repos ?? new ReposWrapper(new Data.DataContext());
			_mapper = mapper;
		}


		//TODO пересмотреть логику
		public bool Contains(GenreFilmsDTO entity) => Get(entity.Film.Name)
			!= null ? true : false;


		public GenreFilmsDTO Create(GenreFilmsDTO entity)
		{
			if (Contains(entity))
				return null;

			return _mapper.Map<GenreFilmsDTO>(_repos.GenreFilms.Create(_mapper.Map<GenreFilms>(entity)));
		}

		public void Delete(int Id)
		{
			var entity = _repos.GenreFilms.Get(x => x.GenreFilmsId == Id);

			if (entity != null)
				_repos.GenreFilms.Delete(entity);
		}

		public void Edit(GenreFilmsDTO entity)
		{

			_repos.GenreFilms.Edit(_mapper.Map<GenreFilms>(entity));
		}

		public IEnumerable<GenreFilmsDTO> FindWithInclude(Func<GenreFilmsDTO, bool> predicate, params Expression<Func<GenreFilmsDTO, object>>[] includeProperties)
		{
			return _mapper.Map<IEnumerable<GenreFilmsDTO>>
				(_repos.GenreFilms.FindWithInclude(_mapper.Map<Func<GenreFilms, bool>>(predicate),
				_mapper.Map<Expression<Func<GenreFilms, object>>>(includeProperties)));
		}

		public GenreFilmsDTO Get(string name) =>
			_mapper.Map<GenreFilmsDTO>(_repos.GenreFilms.Get(x => x.Film.Name == name)) ?? null;

		public async Task<IEnumerable<GenreFilmsDTO>> GetAll() =>
			_mapper.Map<IEnumerable<GenreFilmsDTO>>(await _repos.GenreFilms.GetAll());


		public async Task<IEnumerable<GenreFilmsDTO>> GetAllWithInclude(params Expression<Func<GenreFilmsDTO, object>>[] includeProperties)
		{
			return _mapper.Map<IEnumerable<GenreFilmsDTO>>(await _context.GenreFilms.Include(x => x.Film).Include(x => x.Genre).ToListAsync());
		}

		public GenreFilmsDTO GetWithInclude(string name)
		{
			return null;
		}
	}
}
