﻿using AutoMapper;
using HomeCinema.Data.Entities;
using HomeCinema.Data.Interfaces;
using HomeCinema.Data.Repositories;
using HomeCinema.Services.Dto;
using HomeCinema.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace HomeCinema.Services.Services
{
      public	class GenreService : IService<GenreDTO>
	{
		IReposWrapper _repos;
		IMapper _mapper;

		public GenreService(IMapper mapper, IReposWrapper repos = null)
		{
			_repos = repos ?? new ReposWrapper(new Data.DataContext());
			_mapper = mapper;
		}

		public bool Contains(GenreDTO entity) => Get(entity.Name)
			!= null ? true : false;


		public GenreDTO Create(GenreDTO entity)
		{
			if (Contains(entity))
				return null;

			return _mapper.Map<GenreDTO>(_repos.Genre.Create(_mapper.Map<Genre>(entity)));
		}

		public void Delete(int Id)
		{
			var entity = _repos.Genre.Get(x => x.Id == Id);

			if (entity != null)
				_repos.Genre.Delete(entity);
		}

		public void Edit(GenreDTO entity)
		{
			var ent = Get(entity.Name);

			if (ent != null)
				_repos.Genre.Edit(_mapper.Map<Genre>(ent));
		}

		public IEnumerable<GenreDTO> FindWithInclude(Func<GenreDTO, bool> predicate, params Expression<Func<GenreDTO, object>>[] includeProperties)
		{
			return _mapper.Map<IEnumerable<GenreDTO>>
				(_repos.Genre.FindWithInclude(_mapper.Map<Func<Genre, bool>>(predicate),
				_mapper.Map<Expression<Func<Genre, object>>>(includeProperties)));
		}

		public GenreDTO Get(string name) =>
			_mapper.Map<GenreDTO>(_repos.Genre.Get(x => x.Name == name)) ?? null;

		public async Task<IEnumerable<GenreDTO>> GetAll() =>
			_mapper.Map<IEnumerable<GenreDTO>>(await _repos.Genre.GetAll());


		public async Task<IEnumerable<GenreDTO>> GetAllWithInclude(params Expression<Func<GenreDTO, object>>[] includeProperties)
		{
			return await _mapper.Map<Task<IEnumerable<GenreDTO>>>(_repos.Genre.GetAllWithInclude
				(_mapper.Map<Expression<Func<Genre, object>>>(includeProperties)));
		}

		public GenreDTO GetWithInclude(string name)
		{
			return null;
		}
	}
}
