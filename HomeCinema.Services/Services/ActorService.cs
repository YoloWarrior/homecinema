﻿using AutoMapper;
using HomeCinema.Data.Entities;
using HomeCinema.Data.Interfaces;
using HomeCinema.Data.Repositories;
using HomeCinema.Services.Dto;
using HomeCinema.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace HomeCinema.Services.Services
{
	public class ActorService : IService<ActorDTO>
	{
		IReposWrapper _repos;
		IMapper _mapper;

		public ActorService(IMapper mapper, IReposWrapper repos = null)
		{
			_repos = repos ?? new ReposWrapper(new Data.DataContext());
			_mapper = mapper;
		}

		public bool Contains(ActorDTO entity) => Get(entity.Name)
			!= null ? true : false;


		public ActorDTO Create(ActorDTO entity)
		{
			if (Contains(entity))
				return null;

			return _mapper.Map<ActorDTO>(_repos.Actor.Create(_mapper.Map<Actor>(entity)));
		}

		public void Delete(int Id)
		{
			var entity = _repos.Actor.Get(x => x.Id == Id);

			if (entity != null)
				_repos.Actor.Delete(entity);
		}

		public void Edit(ActorDTO entity)
		{
			var ent = Get(entity.Name);

			if (ent != null)
				_repos.Actor.Edit(_mapper.Map<Actor>(ent));
		}

		public IEnumerable<ActorDTO> FindWithInclude(Func<ActorDTO, bool> predicate, params Expression<Func<ActorDTO, object>>[] includeProperties)
		{
			return _mapper.Map<IEnumerable<ActorDTO>>
				(_repos.Actor.FindWithInclude(_mapper.Map<Func<Actor, bool>>(predicate),
				_mapper.Map<Expression<Func<Actor, object>>>(includeProperties)));
		}

		public ActorDTO Get(string name) =>
			_mapper.Map<ActorDTO>(_repos.Actor.Get(x => x.Name == name)) ?? null;

		public async Task<IEnumerable<ActorDTO>> GetAll() =>
			_mapper.Map<IEnumerable<ActorDTO>>(await _repos.Actor.GetAll());


		public async Task<IEnumerable<ActorDTO>> GetAllWithInclude(params Expression<Func<ActorDTO, object>>[] includeProperties)
		{
			return   _mapper.Map<IEnumerable<ActorDTO>>(_repos.Actor.GetAllWithInclude
				(_mapper.Map<Expression<Func<Actor, object>>>(includeProperties)));
		}

		public ActorDTO GetWithInclude(string name)
		{
			return null;
		}
	}
}
