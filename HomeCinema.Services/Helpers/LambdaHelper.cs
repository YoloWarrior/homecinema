﻿//using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Linq.Expressions;
//using System.Reflection;
//using System.Text;


//Пысы Написание  лямбды,Получить значение из лямбды*(полезно!)

//namespace HomeCinema.Services.Helpers
//{
//	public static class LambdaHelper<T> where T : class
//	{
//		public static Expression<Func<T, object>> GetExpression<TSource>(string propertyName)
//		{
//			var param = Expression.Parameter(typeof(TSource), "x");
//			Expression conversion = Expression.Property(param, propertyName);

//			return Expression.Lambda<Func<T, object>>(conversion, param);
//		}

//		public static Expression<Func<T, bool>> GetDynamicQueryWithExpresionTrees(string propertyName, string val)
//		{
//			var param = Expression.Parameter(typeof(T), "x");

//			#region Convert to specific data type
//			MemberExpression member = Expression.Property(param, propertyName);
//			UnaryExpression valueExpression = GetValueExpression(propertyName, val, param);
//			#endregion

//			Expression body = Expression.Equal(member, valueExpression);
//			var final = Expression.Lambda<Func<T, bool>>(body: body, parameters: param);

//			return final;
//		}

//        public static KeyValuePair<Type, object>[] ResolveArgs<T>(Expression<Func<T, object>> expression)
//        {
//            var body = (System.Linq.Expressions.MethodCallExpression)expression.Body;
//            var values = new List<KeyValuePair<Type, object>>();

//            foreach (var argument in body.Arguments)
//            {
//                var exp = ResolveMemberExpression(argument);
//                var type = argument.Type;

//                var value = GetValue(exp);

//                values.Add(new KeyValuePair<Type, object>(type, value));
//            }

//            return values.ToArray();
//        }

//        public static MemberExpression ResolveMemberExpression(Expression expression)
//        {

//            if (expression is MemberExpression)
//            {
//                return (MemberExpression)expression;
//            }
//            else if (expression is UnaryExpression)
//            {
//                // if casting is involved, Expression is not x => x.FieldName but x => Convert(x.Fieldname)
//                return (MemberExpression)((UnaryExpression)expression).Operand;
//            }
//            else
//            {
//                throw new NotSupportedException(expression.ToString());
//            }
//        }

//        private static object GetValue(MemberExpression exp)
//        {
//            // expression is ConstantExpression or FieldExpression
//            if (exp.Expression is ConstantExpression)
//            {
//                return (((ConstantExpression)exp.Expression).Value)
//                        .GetType()
//                        .GetField(exp.Member.Name)
//                        .GetValue(((ConstantExpression)exp.Expression).Value);
//            }
//            else if (exp.Expression is MemberExpression)
//            {
//                return GetValue((MemberExpression)exp.Expression);
//            }
//            else
//            {
//                throw new NotImplementedException();
//            }
//        }

//        private static UnaryExpression GetValueExpression(string propertyName, string val, ParameterExpression param)
//		{
//			var member = Expression.Property(param, propertyName);
//			var propertyType = ((PropertyInfo)member.Member).PropertyType;
//			var converter = TypeDescriptor.GetConverter(propertyType);

//			if (!converter.CanConvertFrom(typeof(string)))
//				throw new NotSupportedException();

//			var propertyValue = converter.ConvertFromInvariantString(val);
//			var constant = Expression.Constant(propertyValue);

//			return Expression.Convert(constant, propertyType);
//		}
//	}
//}
