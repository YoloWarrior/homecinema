﻿using HomeCinema.Services.Dto;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace HomeCinema.Services.Helpers
{
	public static class GetCollectionOfGenresAndActorsFromRequest
	{
		public static FilmDTO GetFilmDTOFromRequest(FilmDTO film, string actorsJson, string genresJson)
		{
			var actorsFilms = new List<ActorFilmsDTO>();
			var genreFilms = new List<GenreFilmsDTO>();

			var actorsList = JsonConvert.DeserializeObject<IEnumerable<string>>(actorsJson);
			var genreList = JsonConvert.DeserializeObject<IEnumerable<string>>(genresJson);

			foreach (var actor in actorsList)
			{
				actorsFilms.Add(new ActorFilmsDTO
				{
					Actor = new ActorDTO { Name = actor },
					Film = film
				});
			};

			foreach (var genre in genreList)
			{
				genreFilms.Add(new GenreFilmsDTO
				{
					Genre = new GenreDTO { Name = genre },
					Film = film
				});
			}

			return new FilmDTO
			{
				Description = film.Description,
				Name = film.Name,
				Producer = film.Producer,
				YearOfRelease = film.YearOfRelease,
				ActorFilms = actorsFilms,
				GenreFilms = genreFilms
			};
		}
	}
}
