﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HomeCinema.Data.Migrations
{
    public partial class fixFilmEntity2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "YearOfRelease",
                table: "Films",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "YearOfRelease",
                table: "Films",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(int));
        }
    }
}
