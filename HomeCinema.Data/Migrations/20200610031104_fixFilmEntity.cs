﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HomeCinema.Data.Migrations
{
    public partial class fixFilmEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Films_AspNetUsers_UserId1",
                table: "Films");

            migrationBuilder.DropIndex(
                name: "IX_Films_UserId1",
                table: "Films");

            migrationBuilder.DropColumn(
                name: "UserId1",
                table: "Films");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "Films",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier");

            migrationBuilder.CreateIndex(
                name: "IX_Films_UserId",
                table: "Films",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Films_AspNetUsers_UserId",
                table: "Films",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Films_AspNetUsers_UserId",
                table: "Films");

            migrationBuilder.DropIndex(
                name: "IX_Films_UserId",
                table: "Films");

            migrationBuilder.AlterColumn<Guid>(
                name: "UserId",
                table: "Films",
                type: "uniqueidentifier",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserId1",
                table: "Films",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Films_UserId1",
                table: "Films",
                column: "UserId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Films_AspNetUsers_UserId1",
                table: "Films",
                column: "UserId1",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
