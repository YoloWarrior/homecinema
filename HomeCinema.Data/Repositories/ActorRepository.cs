﻿using HomeCinema.Data.Entities;
using HomeCinema.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace HomeCinema.Data.Repositories
{
public	class ActorRepository:GenereticRepository<Actor>,IActor
	{
		public ActorRepository(DataContext context) : base(context) { }
	}
}
