﻿using HomeCinema.Data.Entities;
using HomeCinema.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace HomeCinema.Data.Repositories
{
	public class FilmRepository : GenereticRepository<Film>, IFilm
	{
		public FilmRepository(DataContext context) : base(context) { }
	}
}
