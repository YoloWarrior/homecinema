﻿using HomeCinema.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace HomeCinema.Data.Repositories
{
	public class ReposWrapper : IReposWrapper
	{
		private DataContext _db;
		private readonly IFilm _film;
		private readonly IImage _image;
		private readonly IActor _actor;
		private readonly IGenre _genre;
		private readonly IGenreFilms _genreFilms;
		private readonly IActorFilms _actorFilms;

		public ReposWrapper(DataContext context)
		{
			_db = context;
		}


		public IFilm Film => _film ?? new FilmRepository(_db);

		public IImage Image => _image ?? new ImageRepository(_db);

		public IActor Actor => _actor ?? new ActorRepository(_db);

		public IGenre Genre => _genre ?? new GenreRepository(_db);

		public IGenreFilms GenreFilms => _genreFilms ?? new GenreFilmsRepository(_db);

		public IActorFilms ActorFilms => _actorFilms ?? new ActorFilmsRepository(_db);
	}
}
