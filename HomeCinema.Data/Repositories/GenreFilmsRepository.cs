﻿using HomeCinema.Data.Entities;
using HomeCinema.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace HomeCinema.Data.Repositories
{
public	class GenreFilmsRepository : GenereticRepository<GenreFilms>, IGenreFilms
	{
		public GenreFilmsRepository(DataContext context) : base(context) { }
	}
}
