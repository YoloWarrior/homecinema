﻿using HomeCinema.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace HomeCinema.Data.Repositories
{
	public class GenereticRepository<T> : IGenereticRepos<T>, IGenereticRepositoryWithInclude<T> where T : class, new()
	{
		private DataContext _context;
		private DbSet<T> db;

		public GenereticRepository(DataContext context)
		{
			_context = context;
			db = _context.Set<T>();
		}
		public T Create(T entity)
		{
			db.Add(entity);
			_context.SaveChanges();

			return entity;
		}

		public void Delete(T entity)
		{
			db.Remove(entity);
			_context.SaveChanges();
		}

		public void Edit(T entity)
		{
			_context.Update(entity);
			_context.SaveChanges();
		}

		public  IEnumerable<T> FindWithInclude(Func<T, bool> predicate, params Expression<Func<T, object>>[] includeProperties)
		{
			return IncludeSingle(includeProperties).Where(predicate).ToList();
		}

		public T Get(Func<T, bool> predicate)
		{
			return db.AsNoTracking().FirstOrDefault(predicate);
		}

		public async Task<IEnumerable<T>> GetAll()
		{
			return await _context.Set<T>().ToListAsync();
		}

		public async Task<IEnumerable<T>> GetAllWithInclude(params Expression<Func<T, object>>[] includeProperties)
		{
			return await Include(includeProperties);
		}

		public T GetWithInclude(Func<T, bool>predicate, IEnumerable<Expression<Func<T, object>>> includeProperties)
		{
			return IncludeSingle(includeProperties).Where(predicate).SingleOrDefault();
		}

		private  async Task<IEnumerable<T>> Include(IEnumerable<Expression<Func<T, object>>> includeProperties)
		{
			var query = db.AsQueryable();

			foreach (var include in includeProperties)
				query = query.Include(include);

			return await query.ToListAsync();
		}

		private IEnumerable<T> IncludeSingle(IEnumerable<Expression<Func<T, object>>> includeProperties)
		{
			var query = db.AsQueryable();

			foreach (var include in includeProperties)
				query = query.Include(include);

			return query;
		}


	}
}
