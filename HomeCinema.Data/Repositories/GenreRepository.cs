﻿using HomeCinema.Data.Entities;
using HomeCinema.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace HomeCinema.Data.Repositories
{
	public class GenreRepository : GenereticRepository<Genre>,IGenre
	{
		public GenreRepository(DataContext context) : base(context) { }
	}
}
