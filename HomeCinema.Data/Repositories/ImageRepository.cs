﻿using HomeCinema.Data.Entities;
using HomeCinema.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace HomeCinema.Data.Repositories
{
	public class ImageRepository : GenereticRepository<Image>, IImage
	{
		public ImageRepository(DataContext context) : base(context) { }
	}
}
