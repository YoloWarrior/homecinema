﻿using HomeCinema.Data.Entities;
using HomeCinema.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace HomeCinema.Data.Repositories
{
	public class ActorFilmsRepository : GenereticRepository<ActorFilms>, IActorFilms
	{
		public ActorFilmsRepository(DataContext context) : base(context) { }
	}
}

