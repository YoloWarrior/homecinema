﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeCinema.Data.Entities
{
public	class ActorFilms
	{
		public int ActorFilmsId { get; set; }
		public int ActorId { get; set; }
		public int FilmId { get; set; }
		public virtual Actor Actor { get; set; }
		public virtual Film Film { get; set; }
	}
}
