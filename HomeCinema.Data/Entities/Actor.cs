﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeCinema.Data.Entities
{
	public class Actor
	{
		public int Id { get; set; }
		public string Name { get; set; }

		public virtual IList<ActorFilms> ActorFilms { get; set; }
	}
}
