﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeCinema.Data.Entities
{
public	class Image
	{
		public int ImageId { get; set; }
		public string ImageLink { get; set; }
	}
}
