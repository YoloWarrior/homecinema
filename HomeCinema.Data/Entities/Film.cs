﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeCinema.Data.Entities
{
	public class Film
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public int YearOfRelease { get; set; }
		public string Producer { get; set; }
		public Guid UserId { get; set; }
		public  Image Image { get; set; }
		public virtual  ICollection<ActorFilms> ActorFilms { get; set; }
		public virtual  List<GenreFilms> GenreFilms { get; set; }

	}
}
