﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeCinema.Data.Entities
{
public	class GenreFilms
	{
		public int GenreFilmsId { get; set; }
		public int GenreId { get; set; }
		public int FilmId { get; set; }
		public virtual  Genre Genre { get; set; }
		public virtual  Film Film { get; set; }
	}
}
