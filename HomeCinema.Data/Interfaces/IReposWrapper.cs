﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeCinema.Data.Interfaces
{
public interface IReposWrapper
	{
		IFilm Film { get; }
		IImage Image { get; }
		IGenre Genre { get; }
		IActor Actor { get; }
		IGenreFilms GenreFilms { get; }
		IActorFilms ActorFilms { get; }
	}
}
