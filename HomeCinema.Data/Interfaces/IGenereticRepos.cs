﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace HomeCinema.Data.Interfaces
{
	public interface IGenereticRepos<T> where T : class
	{
		Task<IEnumerable<T>> GetAll();
		T Create(T entity);
		void Delete(T entity);
		void Edit(T entity);
		T Get(Func<T, bool> predicate);
		
	}

	public interface IGenereticRepositoryWithInclude<T>:IGenereticRepos<T>where T : class
	{
		Task<IEnumerable<T>> GetAllWithInclude(params Expression<Func<T, object>>[] includeProperties);
		T GetWithInclude(Func<T, bool> predicate,IEnumerable<Expression<Func<T, object>>> includeProperties);
		IEnumerable<T> FindWithInclude(Func<T, bool> predicate, params Expression<Func<T, object>>[] includeProperties);
	}
}
