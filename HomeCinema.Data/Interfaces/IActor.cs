﻿using HomeCinema.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace HomeCinema.Data.Interfaces
{
	public interface IActor : IGenereticRepositoryWithInclude<Actor>
	{
	}
}
