﻿using HomeCinema.Data.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace HomeCinema.Data
{
	public class DataContext : IdentityDbContext<User>
	{
		public DbSet<Film> Films { get; set; }
		public DbSet<Image> Images { get; set; }
		public DbSet<Genre> Genres { get; set; }
		public DbSet<Actor> Actors { get; set; }
		public DbSet<ActorFilms> ActorFilms { get; set; }
		public DbSet<GenreFilms> GenreFilms { get; set; }

		public DataContext() { }

		public DataContext(DbContextOptions<DataContext> options)
		  : base(options)
		{
			Database.EnsureCreated();
		}

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=homecinema;Trusted_Connection=True");
		}

		protected override void OnModelCreating(ModelBuilder builder)
		{
			builder.Entity<GenreFilms>()
				.HasKey(t => new { t.GenreFilmsId, t.FilmId, t.GenreId });

			builder.Entity<GenreFilms>()
			   .HasOne(a => a.Film)
			   .WithMany(x => x.GenreFilms)
			   .HasForeignKey(x => x.FilmId);

			builder.Entity<GenreFilms>()
			   .HasOne(a => a.Genre)
			   .WithMany(x => x.GenreFilms)
			   .IsRequired(true)
			   .HasForeignKey(x => x.GenreId);

			builder.Entity<ActorFilms>()
			.HasKey(t => new { t.ActorFilmsId, t.FilmId, t.ActorId });

			builder.Entity<ActorFilms>()
			   .HasOne(a => a.Film)
			   .WithMany(x => x.ActorFilms)
			   .HasForeignKey(x => x.FilmId);

			builder.Entity<ActorFilms>()
			   .HasOne(a => a.Actor)
			   .WithMany(x => x.ActorFilms)
				.IsRequired(true)
			   .HasForeignKey(x => x.ActorId);


			base.OnModelCreating(builder);
		}
	}
}
